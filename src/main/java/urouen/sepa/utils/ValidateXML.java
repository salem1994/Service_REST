package urouen.sepa.utils;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import javax.xml.validation.SchemaFactory;
import javax.xml.parsers.*;

import org.apache.commons.io.IOUtils;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;



import org.xml.sax.SAXException;



import java.io.IOException;
import java.io.StringWriter;

public class ValidateXML {

    public boolean validateWithDOM(Object o, String toInstance) throws JAXBException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        try {
            String path = getClass().getClassLoader().getResource("tp1.sepa.01.xsd").getPath().replaceAll("%20", " ");
            factory.setSchema(schemaFactory.newSchema(
                    new Source[]{new StreamSource(path)}
            ));
        } catch (SAXException e) {
            e.printStackTrace();
        }

        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        SimpleErrorHandler handler = new SimpleErrorHandler();

        builder.setErrorHandler(handler);

        try {
            builder.parse(IOUtils.toInputStream(ValidateXML.objectToXML(o, toInstance), "UTF-8"));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return !handler.hasError();

    }


    public static String objectToXML(Object o, String toInstance) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(toInstance);
        Marshaller marshaller = jc.createMarshaller();
        StringWriter sw = new StringWriter();
        marshaller.marshal(o, sw);
        return sw.toString();
    }

}
