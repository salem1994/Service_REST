package urouen.sepa.utils;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SimpleErrorHandler implements ErrorHandler {

	boolean errorOccured = false;

	boolean hasError() {
		return errorOccured;
	}

	public void error(SAXParseException arg0) throws SAXException {
		errorOccured = true;

	}

	public void fatalError(SAXParseException arg0) throws SAXException {
		errorOccured = true;

	}

	public void warning(SAXParseException arg0) throws SAXException {
		errorOccured = true;

	}

}
