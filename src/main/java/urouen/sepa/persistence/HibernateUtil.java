package urouen.sepa.persistence;

/**
 * Created by Adam on 03/04/2017.
 */

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.type.LongType;

import java.util.List;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new AnnotationConfiguration().configure().buildSessionFactory();

        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            sessionFactory = buildSessionFactory();
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }




    /* Method to  READ all the Object */
    public static List<?> listObject(String table) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<?> list = null;
        try {
            tx = session.beginTransaction();
            list = session.createQuery("FROM " + table).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            HibernateUtil.shutdown();
        }
        return list;
    }

    /* Method exucte Query */
    public static List<?> exucteQuery(String query) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<?> list = null;
        try {
            tx = session.beginTransaction();
            list = session.createQuery(query).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            HibernateUtil.shutdown();
        }
        return list;
    }


}