package urouen.sepa.persistence;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import urouen.sepa.model.DrctDbtTxInfType;

import java.util.List;


/**
 * Created by Adam on 05/04/2017.
 */
public class TransactionManager {
    public TransactionManager() {
    }

    public static String addTransaction(DrctDbtTxInfType drctDbtTxInf) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        String id = null;
        try {
            tx = session.beginTransaction();
            List<DrctDbtTxInfType> list = session.createQuery("FROM DrctDbtTxInfType where PmtId='" + drctDbtTxInf.getPmtId() + "'").list();
            if (list.size() == 0) {
                id = (String) session.save(drctDbtTxInf);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            HibernateUtil.shutdown();
        }
        return id;
    }


    /* Method to DELETE an Object in the database */
    public static String deletetx(String n) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        String output = null;
        try {
            tx = session.beginTransaction();
            List<DrctDbtTxInfType> list = session.createQuery("FROM DrctDbtTxInfType where num='" + n + "'").list();
            if (list.size() > 0) {
                DrctDbtTxInfType infType = list.get(0);
                Query q = session.createSQLQuery("delete from rmtinftable where id=?");
                q.setString(0, infType.getPmtId());
                q.executeUpdate();
                q = session.createSQLQuery("delete from drctdbttxinf where PmtId=?");
                q.setString(0, infType.getPmtId());
                q.executeUpdate();
                output = n;
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            HibernateUtil.shutdown();
        }
        return output;
    }
}
