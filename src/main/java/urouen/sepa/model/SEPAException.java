package urouen.sepa.model;

/**
 * Created by Adam on 28/04/2017.
 */

public class SEPAException extends Exception {
    public SEPAException(String message) {
        super(message);
    }
}
