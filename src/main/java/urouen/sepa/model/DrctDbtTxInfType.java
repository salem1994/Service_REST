//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.05 at 01:17:08 AM CEST 
//


package urouen.sepa.model;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for DrctDbtTxInfType complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="DrctDbtTxInfType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://univ.fr/sepa}PmtId"/>
 *         &lt;element ref="{http://univ.fr/sepa}InstdAmt"/>
 *         &lt;element ref="{http://univ.fr/sepa}DrctDbtTx"/>
 *         &lt;element ref="{http://univ.fr/sepa}DbtrAgt"/>
 *         &lt;element ref="{http://univ.fr/sepa}Dbtr"/>
 *         &lt;element ref="{http://univ.fr/sepa}DbtrAcct"/>
 *         &lt;element ref="{http://univ.fr/sepa}RmtInf" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DrctDbtTxInfType", propOrder = {
        "pmtId",
        "instdAmt",
        "drctDbtTx",
        "dbtrAgt",
        "dbtr",
        "dbtrAcct",
        "rmtInf"

})
@Entity
@Table(name = "DrctDbtTxInf")
@XmlRootElement(name = "DrctDbtTxInf")
public class DrctDbtTxInfType {

    @XmlElement(name = "PmtId", required = true)
    @Id
    @Column(name = "pmtId")
    protected String pmtId;

    @XmlTransient
    @Column(name = "num")
    protected String num;
    @XmlElement(name = "InstdAmt", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    protected Amount instdAmt;
    @XmlElement(name = "DrctDbtTx", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    protected CmpsdTx drctDbtTx;
    @XmlElement(name = "DbtrAgt", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    protected CmpsdAgt dbtrAgt;
    @XmlElement(name = "Dbtr", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    protected CmpsdNmd dbtr;
    @XmlElement(name = "DbtrAcct", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    protected CmpsdIdt dbtrAcct;

    @XmlElement(name = "RmtInf")
    @ElementCollection
    @CollectionTable(name = "rmtInftable", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "rmtInf")
    protected List<String> rmtInf;


    /**
     * Gets the value of the pmtId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPmtId() {
        return pmtId;
    }

    /**
     * Sets the value of the pmtId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPmtId(String value) {
        this.pmtId = value;
    }

    /**
     * Gets the value of the instdAmt property.
     *
     * @return possible object is
     * {@link Amount }
     */
    public Amount getInstdAmt() {
        return instdAmt;
    }

    /**
     * Sets the value of the instdAmt property.
     *
     * @param value allowed object is
     *              {@link Amount }
     */
    public void setInstdAmt(Amount value) {
        this.instdAmt = value;
    }

    /**
     * Gets the value of the drctDbtTx property.
     *
     * @return possible object is
     * {@link CmpsdTx }
     */
    public CmpsdTx getDrctDbtTx() {
        return drctDbtTx;
    }

    /**
     * Sets the value of the drctDbtTx property.
     *
     * @param value allowed object is
     *              {@link CmpsdTx }
     */
    public void setDrctDbtTx(CmpsdTx value) {
        this.drctDbtTx = value;
    }

    /**
     * Gets the value of the dbtrAgt property.
     *
     * @return possible object is
     * {@link CmpsdAgt }
     */
    public CmpsdAgt getDbtrAgt() {
        return dbtrAgt;
    }

    /**
     * Sets the value of the dbtrAgt property.
     *
     * @param value allowed object is
     *              {@link CmpsdAgt }
     */
    public void setDbtrAgt(CmpsdAgt value) {
        this.dbtrAgt = value;
    }

    /**
     * Gets the value of the dbtr property.
     *
     * @return possible object is
     * {@link CmpsdNmd }
     */
    public CmpsdNmd getDbtr() {
        return dbtr;
    }

    /**
     * Sets the value of the dbtr property.
     *
     * @param value allowed object is
     *              {@link CmpsdNmd }
     */
    public void setDbtr(CmpsdNmd value) {
        this.dbtr = value;
    }

    /**
     * Gets the value of the dbtrAcct property.
     *
     * @return possible object is
     * {@link CmpsdIdt }
     */
    public CmpsdIdt getDbtrAcct() {
        return dbtrAcct;
    }

    /**
     * Sets the value of the dbtrAcct property.
     *
     * @param value allowed object is
     *              {@link CmpsdIdt }
     */
    public void setDbtrAcct(CmpsdIdt value) {
        this.dbtrAcct = value;
    }

    /**
     * Gets the value of the rmtInf property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rmtInf property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRmtInf().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getRmtInf() {
        if (rmtInf == null) {
            rmtInf = new ArrayList<String>();
        }
        return this.rmtInf;
    }

    public void setRmtInf(List<String> rmtInf) {
        this.rmtInf = rmtInf;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
