package urouen.sepa.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Adam on 28/04/2017.
 */

@XmlRootElement(name = "ErrorOverview")
public class ErrorOverview {


    private String message;

    public ErrorOverview() {
    }

    public ErrorOverview(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    @XmlElement
    public void setMessage(String message) {
        this.message = message;
    }
}
