package urouen.sepa.model;


import org.hibernate.annotations.Immutable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javax.persistence.*;


@XmlRootElement(name = "SEPA")
@Entity
@Immutable
@Table(name = "SEPA")
public class SEPA {

    @Id
    @Column(name = "transaction")
    int transaction;

    @Column(name = "montant")
    int montant;

    public SEPA() {
    }

    public SEPA(int transaction, int montant) {
        this.transaction = transaction;
        this.montant = montant;
    }

    public int getTransaction() {
        return transaction;
    }

    @XmlElement
    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }

    public int getMontant() {
        return montant;
    }

    @XmlElement
    public void setMontant(int montant) {
        this.montant = montant;
    }
}