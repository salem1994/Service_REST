package urouen.sepa.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adam on 05/04/2017.
 */
public class Utils {
    public static Object createObject() {
        DrctDbtTxInfType drctDbtTxInfType = new DrctDbtTxInfType();
        String pmtId = "REF OPE AAAA";
        List<String> rmtInf = Arrays.asList("girafe", "chameau", "chat", "poisson", "cachalot");
        drctDbtTxInfType.setPmtId(pmtId);
        drctDbtTxInfType.setRmtInf(rmtInf);


        Amount instdAmt = new Amount();
        instdAmt.setCcy("EURO");
        instdAmt.setValue(BigDecimal.ONE);
        drctDbtTxInfType.setInstdAmt(instdAmt);


        MndtRltdInfType mndtRltdInf = new MndtRltdInfType();
        mndtRltdInf.setMndtId("MANDAT NO 55555");
        //mndtRltdInf.setDtOfSgntr(new Date(2013,3,3));


        CmpsdTx drctDbtTx = new CmpsdTx();
        drctDbtTx.setMndtRltdInf(mndtRltdInf);
        drctDbtTxInfType.setDrctDbtTx(drctDbtTx);


        CmpsdAgt dbtrAgt = new CmpsdAgt();
        FinInstnIdType finInstnIdType = new FinInstnIdType();

        OthrType1 othrType1 = new OthrType1();
        othrType1.setId("NOTPROVIDED");
        finInstnIdType.setOthr(othrType1);

        dbtrAgt.setFinInstnId(finInstnIdType);

        drctDbtTxInfType.setDbtrAgt(dbtrAgt);


        CmpsdNmd dbtr = new CmpsdNmd();
        dbtr.setNm("Mr Debiteur N1");
        drctDbtTxInfType.setDbtr(dbtr);

        CmpsdIdt dbtrAcct = new CmpsdIdt();
        IdType idType = new IdType();
        idType.setIban("GB29NWBK60161331926819");
        drctDbtTxInfType.setDbtrAcct(dbtrAcct);
        return drctDbtTxInfType;
    }
}
