package urouen.sepa.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Adam on 23/04/2017.
 */

@XmlRootElement(name = "resume")
public class Resume {

    private String num;
    private String id;
    private BigDecimal montant;
    private String date;


    public Resume() {

    }

    public Resume(String num, String id, BigDecimal montant, String date) {
        this.num = num;
        this.id = id;
        this.montant = montant;
        this.date = date;
    }

    public String getNum() {
        return num;
    }

    @XmlElement
    public void setNum(String num) {
        this.num = num;
    }

    public String getId() {
        return id;
    }

    @XmlElement
    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    @XmlElement
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getDate() {
        return date;
    }

    @XmlElement
    public void setDate(String date) {
        this.date = date;
    }
}
