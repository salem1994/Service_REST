package urouen.sepa.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by Adam on 23/04/2017.
 */

@XmlRootElement
public class ListResume {


    Collection<Resume> resumes;

    public ListResume() {
    }

    public ListResume(Collection<Resume> resumes) {
        this.resumes = resumes;
    }

    public Collection<Resume> getResumes() {
        return resumes;
    }

    public void setResumes(Collection<Resume> resumes) {
        this.resumes = resumes;
    }
}
