package urouen.sepa.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import urouen.sepa.model.ErrorOverview;
import urouen.sepa.model.SEPAException;

/**
 * Created by Adam on 28/04/2017.
 */

@ControllerAdvice
public class SEPAControllerExceptionHandler {
    @ExceptionHandler(SEPAException.class)
    public ResponseEntity<?> defaultErrorHandler(SEPAException ex) {
        return new ResponseEntity<ErrorOverview>(new ErrorOverview(ex.getMessage()), HttpStatus.OK);
    }
}
