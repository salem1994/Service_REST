package urouen.sepa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class HomeController {
    @RequestMapping(value="/", method = RequestMethod.GET)
    public @ResponseBody String home() {
        return "<center> <h1> \"Welcome SEPA\", Projet : Service REST</h1><p> <b> Auteurs : </b> <b> Mohammed EL HAMMOUMI </b> And <b> Salem ESSAMAOUAL </b> </p> <br> <p> <b> date du projet : </b> 30/04/2017 </p> </center>";
    }
}