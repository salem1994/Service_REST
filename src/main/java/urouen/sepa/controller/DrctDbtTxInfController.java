package urouen.sepa.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import urouen.sepa.model.*;
import urouen.sepa.persistence.HibernateUtil;
import urouen.sepa.persistence.TransactionManager;
import urouen.sepa.utils.ValidateXML;

import javax.xml.bind.JAXBException;
import javax.xml.ws.http.HTTPException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Adam on 06/04/2017.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class DrctDbtTxInfController {


    @RequestMapping(value = "/trx/{n}", method = RequestMethod.GET)
    public ResponseEntity<?> getTransactionInXML(@PathVariable("n") String n) throws SEPAException {
        List<DrctDbtTxInfType> output = (List<DrctDbtTxInfType>) HibernateUtil.exucteQuery("FROM DrctDbtTxInfType where num='" + n + "'");
        if (output.size() == 0)
            throw new SEPAException(" Transaction " + n + " not found  ");
        else
            return new ResponseEntity<DrctDbtTxInfType>(output.get(0), HttpStatus.OK);
    }

    @RequestMapping(value = "/del/{n}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTransaction(@PathVariable("n") String n) throws SEPAException {
        String output = TransactionManager.deletetx(n);
        if (output == null)
            throw new SEPAException(" Transaction " + n + " not exist  ");
        else
            return new ResponseEntity<String>(output, HttpStatus.OK);
    }

    @RequestMapping(value = "/depo", method = RequestMethod.POST)
    public ResponseEntity<?> addTransaction(@RequestBody DrctDbtTxInfType drctDbtTxInfType) throws SEPAException, JAXBException {
        if (new ValidateXML().validateWithDOM(drctDbtTxInfType, "urouen.sepa.model")) {
            String id = TransactionManager.addTransaction(drctDbtTxInfType);
            if (id == null)
                throw new SEPAException(" Transaction " + drctDbtTxInfType.getPmtId() + " exist  ");
            else
                return new ResponseEntity<String>(id, HttpStatus.OK);
        } else {
            throw new SEPAException("Shema not valide");
        }
    }


    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public SEPA dispalySEPA() throws SEPAException {
        return (SEPA) HibernateUtil.exucteQuery("FROM SEPA").get(0);
    }

    @RequestMapping(value = "/resume", method = RequestMethod.GET)
    public ListResume getAllTransactionInXML() {
        LinkedList<Resume> resumes = new LinkedList<Resume>();
        List<DrctDbtTxInfType> output = (List<DrctDbtTxInfType>) HibernateUtil.listObject("DrctDbtTxInfType");
        for (DrctDbtTxInfType drc : output) {
            resumes.add(new Resume(drc.getNum(), drc.getPmtId(), drc.getInstdAmt().getValue(), drc.getDrctDbtTx().getMndtRltdInf().getDtOfSgntr()));
        }
        return new ListResume(resumes);
    }


    //test
    @RequestMapping(value = "/v", method = RequestMethod.POST)
    public ResponseEntity<?> validateXML(@RequestBody DrctDbtTxInfType drctDbtTxInfType) throws JAXBException {
        String str = "NOTOK";
        if (new ValidateXML().validateWithDOM(drctDbtTxInfType, "urouen.sepa.model")) {
            str = "OK";
        }
        return new ResponseEntity<String>(str, HttpStatus.OK);
    }

}